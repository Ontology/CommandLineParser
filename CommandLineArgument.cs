﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CommandLineParser
{
    public enum ArgumentType
    {
        String = 0,
        Bool = 1,
        Long = 2,
        Double = 4,
        DateTime = 8
    }

    public class CommandLineArgument
    {
        public string Name { get; set; }
        
        public string RegexValueCheck { get; set; }
        public ArgumentType ArgumentType { get; set; }

        public string Value { get; set; }
        public bool? BoolVal { get; set; }
        public DateTime? DateTimeVal { get; set; }
        public Double? DoubleVal { get; set; }
        public long? LongVal { get; set; }

        public bool IsArgValid()
        {
            var result = false;
            switch(ArgumentType)
            {
                case ArgumentType.Bool:
                    bool checkBool;
                    result = bool.TryParse(Value, out checkBool);
                    if (result) BoolVal = checkBool;
                    break;
                case ArgumentType.DateTime:
                    DateTime checkDateTime;
                    result = DateTime.TryParse(Value, out checkDateTime);
                    if (result) DateTimeVal = checkDateTime;
                    break;
                case ArgumentType.Double:
                    double checkDateDouble;
                    result = Double.TryParse(Value, out checkDateDouble);
                    if (result) DoubleVal = checkDateDouble;
                    break;
                case ArgumentType.Long:
                    long checkDateLong;
                    result = long.TryParse(Value, out checkDateLong);
                    if (result) LongVal = checkDateLong;
                    break;
                case ArgumentType.String:
                    result = true;
                    break;
            }

            if (result)
            {
                if (!string.IsNullOrEmpty(RegexValueCheck))
                {
                    try
                    {
                        var patternItem = new Regex(RegexValueCheck);
                        result = patternItem.Match(Value).Success;

                    }
                    catch(Exception ex)
                    {
                        result = false;
                    }
                    
                }
            }

            return result;
        }

        public CommandLineArgument(ArgumentType argumentType)
        {
            ArgumentType = argumentType;
        }
        public CommandLineArgument(CommandLineArgument cmdLineArg)
        {
            Name = cmdLineArg.Name;
            Value = cmdLineArg.Value;
            ArgumentType = cmdLineArg.ArgumentType;
            RegexValueCheck = cmdLineArg.RegexValueCheck;
        }
    }
}
