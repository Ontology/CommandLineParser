﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineParser
{
    public class ArgumentParser
    {
        public List<CommandLineArgument> ArgumentItems { get; private set; }

        public bool AddArgument(string argName, ArgumentType argType, string regexForCheck = null)
        {
            if (string.IsNullOrEmpty(argName)) return false;

            ArgumentItems.Add(new CommandLineArgument(argType) { Name = argName, RegexValueCheck = regexForCheck });

            return true;
        }

        public bool AddValue(string argName, string value)
        {
            var argument = ArgumentItems.FirstOrDefault(arg => arg.Name.ToLower() == argName);
            if (argument == null) return false;

            argument.Value = value;

            return true;
        }

        public bool AddValue(int ix, string value)
        {
            if (ArgumentItems.Count >= ix) return false;

            var argument = ArgumentItems[ix];

            argument.Value = value;

            return true;
        }

        public bool AddArguments(string[] arguments, string nameValueSeperator = null)
        {
            if (string.IsNullOrEmpty(nameValueSeperator))
            {

                if (!ArgumentItems.Any())
                {
                    ArgumentItems = arguments.Select(arg => new CommandLineArgument(ArgumentType.String) { Value = arg }).ToList();
                }
                else if (arguments.Length == ArgumentItems.Count)
                {
                    for (int ix = 0; ix < arguments.Length; ix++)
                    {
                        ArgumentItems[ix].Value = arguments[ix];
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                var splittedArguments = arguments.Select(arg => arg.Split(nameValueSeperator.ToCharArray())).ToList();

                if (splittedArguments.Any(spArg => spArg.Length != 2)) return false;

                if (!ArgumentItems.Any())
                {
                    ArgumentItems = splittedArguments.Select(spArg => new CommandLineArgument(ArgumentType.String) { Name = spArg[0], Value = spArg[1] }).ToList();
                }
                else if (splittedArguments.Count == ArgumentItems.Count)
                {

                    ArgumentItems = (from argItem in ArgumentItems
                                     from arg in splittedArguments
                                     where argItem.Name.ToLower() == arg[0].ToLower()
                                     select new CommandLineArgument(argItem) { Value = arg[1] }).ToList();
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        public ArgumentParser()
        {
            ArgumentItems = new List<CommandLineArgument>();
        }
    }
}
